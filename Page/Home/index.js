import React, {Component} from "react";
import {
  AppRegistry,
  Image,
  Text,
  View,
  StyleSheet,
  TextInput,
  Button,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import notif from "../../assets/img/notif.png";
import profil from "../../assets/img/man.jpg";
import cardHome from "../../assets/img/ch.png";
import plus from "../../assets/img/Plus.png";
import arrow from "../../assets/img/arrow.png";

const Home = ({navigation}) => {
  return (
    <ScrollView>
      <View style={{backgroundColor: "white"}}>
        <View style={{flexDirection: "row"}}>
          <TouchableOpacity onPress={() => navigation.navigate("ListNotif")}>
            <Image source={notif} style={styles.notif} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate("profilFix")}>
            <Image source={profil} style={styles.profil} />
          </TouchableOpacity>
        </View>
        <Text style={styles.salam}>Hai, Faiz</Text>
        <View style={styles.container}>
          <ImageBackground source={cardHome} style={styles.cardHome}>
            <Text style={[styles.text, {color: "#FFFFFF", marginTop: 16}]}>
              085712345678
            </Text>
            <View style={{flexDirection: "row"}}>
              <Text
                style={[
                  styles.text,
                  {fontSize: 24, color: "#FFFFFF", marginTop: 5},
                ]}
              >
                Ajukan Klaim
              </Text>
              <TouchableOpacity onPress={() => navigation.navigate("ajuan")}>
                <Image source={plus} style={styles.plus} />
              </TouchableOpacity>
            </View>
            <View>
              {/* <Text style={[{color: '#1F1F1F', padding: 10, }]}>Riwayat Pengajuan </Text> */}
              <TouchableOpacity onPress={() => navigation.navigate("riwayat")}>
                <View style={styles.buttonWrapper}>
                  <Text style={styles.btn}>Riwayat Pengajuan</Text>
                  <Image source={arrow} style={styles.img} />
                </View>
              </TouchableOpacity>
              {/* <Icon name="home-outline" size={10} /> */}
            </View>
          </ImageBackground>
        </View>
        <View style={{flexDirection: "row"}}>
          <Text style={styles.status}>Status</Text>
          <TouchableOpacity onPress={() => navigation.navigate("listKlaim")}>
            <Text
              style={[
                styles.status,
                {marginLeft: 180, fontSize: 12, marginTop: 14},
              ]}
            >
              lihat semua
            </Text>
          </TouchableOpacity>
        </View>

        <View style={{marginbottom: 100}}>
          <TouchableOpacity
            onPress={() => navigation.navigate("detailPengajuanDiproses")}
          >
            <View style={styles.cardStatus}>
              <Text style={styles.cardStatusKolom1}>ID Pengajuan</Text>

              <Text style={[styles.lihatDetail, {color: "#F2C94C"}]}>
                Lihat Detail
              </Text>

              <Text style={styles.cardStatusKolom1}>#100</Text>
              <Text style={{marginLeft: 210, marginTop: -20}}>Rp100.000</Text>
              <Text style={styles.cardStatusKolom1} ac>
                10 Desember 2020
              </Text>
              <Text style={[styles.lihatDetail, {fontWeight: "bold"}]}>
                Proses
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate("detailPengajuanDiproses")}
          >
            <View style={styles.cardStatus}>
              <Text style={styles.cardStatusKolom1}>ID Pengajuan</Text>

              <Text style={[styles.lihatDetail, {color: "#F2C94C"}]}>
                Lihat Detail
              </Text>

              <Text style={styles.cardStatusKolom1}>#100</Text>
              <Text style={{marginLeft: 210, marginTop: -20}}>Rp100.000</Text>
              <Text style={styles.cardStatusKolom1} ac>
                10 Desember 2020
              </Text>
              <Text style={[styles.lihatDetail, {fontWeight: "bold"}]}>
                Proses
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate("detailPengajuanDiproses")}
          >
            <View style={[styles.cardStatus, {marginbottom: 100}]}>
              <Text style={styles.cardStatusKolom1}>ID Pengajuan</Text>

              <Text style={[styles.lihatDetail, {color: "#F2C94C"}]}>
                Lihat Detail
              </Text>

              <Text style={styles.cardStatusKolom1}>#100</Text>
              <Text style={{marginLeft: 210, marginTop: -20}}>Rp100.000</Text>
              <Text style={styles.cardStatusKolom1} ac>
                10 Desember 2020
              </Text>
              <Text style={[styles.lihatDetail, {fontWeight: "bold"}]}>
                Proses
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  img: {
    marginTop: -40,
    width: 20,
    height: 20,
    marginLeft: 230,
    justifyContent: "center",
  },
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    borderRadius: 40,
  },
  notif: {
    width: 16,
    height: 19,
    marginLeft: 24,
    marginTop: 54,
  },
  profil: {
    width: 50,
    height: 50,
    borderRadius: 50,
    marginLeft: 250,
    marginTop: 35,
  },
  salam: {
    fontFamily: "Poppins-Bold",
    fontSize: 32,
    color: "#1F1F1F",
    marginTop: 10,
    marginLeft: 20,
  },
  cardHome: {
    marginTop: 1,
    width: 308,
    height: 190,
    // borderRadius: 50,
    marginLeft: -7,
  },
  btn: {
    backgroundColor: "#fff",
    width: 250,
    height: 56,
    borderRadius: 18,
    marginLeft: 26,
    flexDirection: "row",
    alignItems: "center",
    marginTop: 10,
    padding: 15,
  },
  status: {
    marginTop: 10,
    marginLeft: 24,
    fontFamily: "Poppins-Regular",
    fontSize: 16,
  },
  lihatDetail: {
    marginTop: -16,
    marginLeft: 210,
    fontSize: 14,
    fontFamily: "Popins-Regular",
  },
  cardStatus: {
    flexDirection: "column",
    marginLeft: 24,
    marginTop: 14,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: "#BFBFBF",
    width: 308,
    height: 108,
  },
  cardStatusKolom1: {
    marginLeft: 10,
    marginTop: 10,
    color: "black",
  },
  text: {
    marginLeft: 30,
  },
  plus: {
    backgroundColor: "#FFC200",
    width: 32,
    height: 32,
    justifyContent: "center",
    borderRadius: 30 / 2,
    padding: 5,
    marginLeft: 72,
    marginTop: 8,
  },
});

export default Home;
