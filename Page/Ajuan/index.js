import React, {Component} from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  YellowBox,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  Menu,
  MenuProvider,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from "react-native-popup-menu";
import backIcon from "../../assets/img/backIcon.png";

const Ajuan = ({navigation}) => {
  return (
    <ScrollView style={{backgroundColor: "white"}}>
      <MenuProvider style={{flexDirection: "column", padding: 30}}>
        <TouchableOpacity
          onPress={() => navigation.navigate("home")}
          style={{flexDirection: "row"}}
        >
          <View style={styles.buttonWrapper}>
            <Image source={backIcon} style={styles.img} />
          </View>
          <Text
            style={[styles.text, {fontSize: 14, marginLeft: 14, marginTop: 40}]}
          >
            Detail Pengajuan
          </Text>
        </TouchableOpacity>
        <Text style={[styles.text, {marginLeft: 8, marginTop: 10}]}>
          Pengajuan{"\n"}Klaim
        </Text>
        <Text
          title="jenisKlaim"
          style={[
            styles.text,
            {fontSize: 14, marginTop: 10, marginLeft: 10, marginBottom: 8},
          ]}
        >
          Jenis Klaim
        </Text>
        <Menu onSelect={(value) => alert(`You Clicked : ${value}`)}>
          {/* <Menu onSelect={value => alert(`You Clicked : ${value}`)}> */}

          <MenuTrigger style={styles.txtInput}></MenuTrigger>

          <MenuOptions>
            <MenuOption value={"KlaimMedis"}>
              <Text style={styles.menuContent}>Klaim Medis</Text>
            </MenuOption>
            <MenuOption value={"KlaimDinas"}>
              <Text style={styles.menuContent}>Klamin Dinas</Text>
            </MenuOption>
            <MenuOption value={"KlaimPulsa"}>
              <Text style={styles.menuContent}>Klaim Pulsa</Text>
            </MenuOption>
            <MenuOption value={"KlaimTransportasi"}>
              <Text style={styles.menuContent}>Klaim Transportasi</Text>
            </MenuOption>
            <MenuOption value={3} disabled={true}>
              <Text style={styles.menuContent}>Disabled Menu</Text>
            </MenuOption>
          </MenuOptions>
          <Text
            style={[
              styles.text,
              {fontSize: 14, marginTop: 15, marginLeft: 10, marginBottom: 8},
            ]}
          >
            Jumlah
          </Text>
          <TextInput editable maxLength={40} style={styles.txtInput} />
          <Text
            style={[
              styles.text,
              {fontSize: 14, marginTop: 15, marginLeft: 10, marginBottom: 8},
            ]}
          >
            Vendor
          </Text>
          <TextInput editable maxLength={40} style={styles.txtInput} />
          <Text
            style={[
              styles.text,
              {fontSize: 14, marginTop: 15, marginLeft: 10, marginBottom: 8},
            ]}
          >
            Alamat
          </Text>
          <TextInput editable maxLength={40} style={styles.txtInput} />
          <Text
            style={[
              styles.text,
              {fontSize: 14, marginTop: 15, marginLeft: 10, marginBottom: 8},
            ]}
          >
            Tanggal
          </Text>
          <Text
            style={[
              styles.text,
              {fontSize: 14, marginTop: 15, marginLeft: 10, marginBottom: 8},
            ]}
          >
            Deskripsi Klaim
          </Text>
          <TextInput editable maxLength={40} style={styles.txtInputDeskripsi} />
          <Text
            style={[
              styles.text,
              {fontSize: 14, marginTop: 30, marginLeft: 10, marginBottom: 8},
            ]}
          >
            Foto
          </Text>
          <View>
            <View style={{flexDirection: "row"}}>
              <Text style={styles.ambilFoto}>Ambil Foto</Text>
              <Text style={[styles.ambilFoto, {marginLeft: 30, marginTop: 8}]}>
                Upload Gambar
              </Text>
            </View>
          </View>
        </Menu>
      </MenuProvider>
    </ScrollView>
  );
};

export default Ajuan;

const styles = StyleSheet.create({
  img: {
    marginTop: 40,
    width: 24,
    height: 24,
    marginLeft: 10,
  },
  headerText: {
    padding: 15,
    borderRadius: 12,
    fontSize: 14,
    marginLeft: 10,
    fontFamily: "Poppins-Regular",
    backgroundColor: "#F1F1F1",
  },
  menuContent: {
    fontFamily: "Poppins-Regular",
    color: "#000",
    padding: 2,
    fontSize: 14,
  },
  text: {
    fontFamily: "Poppins-Regular",
    fontSize: 30,
    color: "#1F1F1F",
  },
  txtInput: {
    fontFamily: "Poppins-Regular",
    backgroundColor: "#f1f1f1",
    borderRadius: 12,
    marginLeft: 10,
    height: 40,
    padding: 15,
    fontSize: 14,

    flexDirection: "column",
    marginLeft: 10,
    marginTop: 14,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: "#BFBFBF",
    width: 270,
    height: 50,
  },
  txtInputDeskripsi: {
    fontFamily: "Poppins-Regular",
    backgroundColor: "#f1f1f1",
    borderRadius: 12,
    marginLeft: 10,
    height: 200,
    padding: 15,
    fontSize: 14,

    flexDirection: "column",
    marginLeft: 10,
    marginTop: 14,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: "#BFBFBF",
    width: 270,
    height: 120,
  },
  ambilFoto: {
    width: 120,
    height: 100,
    backgroundColor: "#fff",
    borderRadius: 12,
    padding: 10,
    textAlign: "center",
    alignContent: "center",
    alignItems: "center",
    justifyContent: "space-around",
    marginLeft: 24,
    marginTop: 8,
    elevation: 3.5,
  },
  status: {
    marginTop: 10,
    marginLeft: 24,
    fontFamily: "Poppins-Regular",
    fontSize: 16,
  },
  cardStatus: {
    flexDirection: "column",
    marginLeft: 10,
    marginTop: 14,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: "#BFBFBF",
    width: 270,
    height: 60,
  },
});
