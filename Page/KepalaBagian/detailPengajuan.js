import React, {Component} from "react";
import nota from "./assets/img/nota.jpg";
import {
  AppRegistry,
  Image,
  Text,
  View,
  StyleSheet,
  TextInput,
  Button,
  ImageBackground,
  ScrollView,
} from "react-native";
import Icon from "react-native-ionicons";
import notif from "./assets/img/notif.png";
import profil from "./assets/img/man.jpg";
import cardHome from "./assets/img/ch.png";
// import Icon from 'react-native-vector-icons/Ionicons';
import Lihat from "./assets/img/chevron-forward-outline.svg";
import {assets} from "../react-native.config";

const App = () => {
  return (
    <View style={{backgroundColor: "white"}}>
      <View>
        <Text style={styles.text}>Detail Pengajuan</Text>
        <Text
          style={[
            styles.text,
            {fontSize: 30, marginLeft: 46, justifyContent: "center"},
          ]}
        >
          Detail {"\n"}Pengajuan
        </Text>
      </View>
      <View
        style={{
          marginTop: 27,
          marginHorizontal: 30,
          marginBottom: 30,
          padding: 20,
          borderRadius: 12,
          elevation: 3.5,
          width: 297,
          height: 370,
        }}
      >
        <Text style={styles.contentTitle}>Jenis Klaim</Text>
        <Text style={styles.content}>Kesehatan </Text>
        <Text style={styles.contentTitle}>Nama Karyawan</Text>
        <Text style={styles.content}>Faiz</Text>
        <Text style={styles.contentTitle}>Tanggal Pengajuan</Text>
        <Text style={styles.content}>10 Des 2020</Text>
        <Text style={styles.contentTitle}>Jumlah</Text>
        <Text style={styles.content}>Rp300.000</Text>
        <Text style={styles.contentTitle}>Vendor</Text>
        <Text style={styles.content}>Starbucks</Text>
        <Text style={styles.contentTitle}>Alamat</Text>
        <Text style={styles.content}>Jawa Tengah</Text>
        <Text style={styles.contentTitle}>Nota</Text>
        <Image source={nota} style={{width: 30, height: 30}} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontFamily: "Poppins-Regular",
    fontSize: 14,
    color: "#1F1F1F",
    marginTop: 36,
    marginLeft: 81,
  },
  contentTitle: {
    color: "#1F1F1F",
    fontSize: 14,
    fontFamily: "Poppins-Regular",
    marginTop: 8,
  },
  content: {
    color: "#828282",
    fontSize: 14,
    fontFamily: "Poppins-Regular",
    marginTop: 4,
  },
});

export default App;
