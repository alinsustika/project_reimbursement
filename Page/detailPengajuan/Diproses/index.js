import React, {Component} from "react";
import nota from "../../../assets/img/nota.jpg";
import {
  AppRegistry,
  Image,
  Text,
  View,
  StyleSheet,
  TextInput,
  Button,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import backIcon from "../../../assets/img/backIcon.png";
import ellipse from "../../../assets/img/Ellipse.png";

const detailPengajuan = ({navigation}) => {
  return (
    <ScrollView>
      <View style={{backgroundColor: "white"}}>
        <View>
          <TouchableOpacity
            onPress={() => navigation.navigate("home")}
            style={{flexDirection: "row"}}
          >
            <View style={styles.buttonWrapper}>
              <Image source={backIcon} style={styles.img} />
            </View>
            <Text
              style={[
                styles.text,
                {fontSize: 14, marginLeft: 14, marginTop: 42},
              ]}
            >
              Detail Pengajuan
            </Text>
          </TouchableOpacity>
          <Text
            style={[
              styles.text,
              {
                fontSize: 30,
                marginLeft: 30,
                justifyContent: "center",
                marginBottom: 20,
              },
            ]}
          >
            Pengajuan
          </Text>
        </View>
        <View
          style={{
            marginTop: 5,
            marginHorizontal: 30,
            marginBottom: 30,
            padding: 20,
            borderRadius: 12,
            elevation: 1,
            width: 297,
            height: 1300,
          }}
        >
          <Text style={[styles.header, {marginTop: 10}]}>
            Status Persetujuan Klaim{" "}
          </Text>
          <View style={styles.cardStatus}>
            <TouchableOpacity
              onPress={() => navigation.navigate("home")}
              style={{flexDirection: "row"}}
            >
              <View style={styles.buttonWrapper}>
                <Image source={ellipse} style={styles.ellipse} />
              </View>
            </TouchableOpacity>
            <View style={[styles.wrap, {marginBottom: 0}]}>
              <Text style={[styles.cardStatusKolom1, {marginLeft: 35}]}>
                Yosi (Admin)
              </Text>
              <TouchableOpacity
                onPress={() => navigation.navigate("detailPengajuan")}
              >
                <Text
                  style={[styles.lihatDetail, {marginLeft: 35, color: "red"}]}
                >
                  Menunggu Konfirmasi
                </Text>
              </TouchableOpacity>
              <Text
                style={[
                  styles.cardStatusKolom1,
                  {color: "#828282", marginLeft: 35},
                ]}
              >
                10 Des 2020
              </Text>
              <Text
                style={[
                  styles.cardStatusKolom1,
                  {color: "#828282", marginTop: -20, marginLeft: 140},
                ]}
              >
                14:00
              </Text>
            </View>

            <TouchableOpacity
              onPress={() => navigation.navigate("home")}
              style={{flexDirection: "row"}}
            >
              <View style={styles.buttonWrapper}>
                <Image source={ellipse} style={styles.ellipse} />
              </View>
            </TouchableOpacity>
            <View style={[styles.wrap, {marginBottom: 0}]}>
              <Text
                style={[
                  styles.cardStatusKolom1,
                  {color: "#828282", marginLeft: 35},
                ]}
              >
                Puput Andara(HRD)
              </Text>
              <TouchableOpacity
                onPress={() => navigation.navigate("detailPengajuan")}
              >
                <Text
                  style={[
                    styles.lihatDetail,
                    {color: "#828282", marginLeft: 35},
                  ]}
                >
                  Klaim Disetujui
                </Text>
              </TouchableOpacity>
              <Text
                style={[
                  styles.cardStatusKolom1,
                  {color: "#828282", marginLeft: 35},
                ]}
              >
                10 Des 2020
              </Text>
              <Text
                style={[
                  styles.cardStatusKolom1,
                  {color: "#828282", marginTop: -20, marginLeft: 140},
                ]}
              >
                14:00
              </Text>
            </View>

            <TouchableOpacity
              onPress={() => navigation.navigate("home")}
              style={{flexDirection: "row"}}
            >
              <View style={styles.buttonWrapper}>
                <Image source={ellipse} style={styles.ellipse} />
              </View>
            </TouchableOpacity>
            <View style={[styles.wrap, {marginBottom: 0}]}>
              <Text
                style={[
                  styles.cardStatusKolom1,
                  {color: "#828282", marginLeft: 35},
                ]}
              >
                Wawan(Kepala Divisi Web/Apps)
              </Text>
              <TouchableOpacity
                onPress={() => navigation.navigate("detailPengajuan")}
              >
                <Text
                  style={[
                    styles.lihatDetail,
                    {color: "#828282", marginLeft: 35},
                  ]}
                >
                  Klaim Disetujui
                </Text>
              </TouchableOpacity>
              <Text
                style={[
                  styles.cardStatusKolom1,
                  {color: "#828282", marginLeft: 35},
                ]}
              >
                10 Des 2020
              </Text>
              <Text
                style={[
                  styles.cardStatusKolom1,
                  {color: "#828282", marginTop: -20, marginLeft: 140},
                ]}
              >
                14:00
              </Text>
            </View>

            <TouchableOpacity
              onPress={() => navigation.navigate("home")}
              style={{flexDirection: "row"}}
            >
              <View style={styles.buttonWrapper}>
                <Image source={ellipse} style={styles.ellipse} />
              </View>
            </TouchableOpacity>
            <View style={[styles.wrap, {marginBottom: 0}]}>
              <Text
                style={[
                  styles.cardStatusKolom1,
                  {color: "#828282", marginLeft: 35},
                ]}
              >
                Faiz Ferdian
              </Text>
              <TouchableOpacity
                onPress={() => navigation.navigate("detailPengajuan")}
              >
                <Text
                  style={[
                    styles.lihatDetail,
                    {color: "#828282", marginLeft: 35},
                  ]}
                >
                  Klaim Diajukan
                </Text>
              </TouchableOpacity>
              <Text
                style={[
                  styles.cardStatusKolom1,
                  {color: "#828282", marginLeft: 35},
                ]}
              >
                10 Des 2020
              </Text>
              <Text
                style={[
                  styles.cardStatusKolom1,
                  {color: "#828282", marginTop: -20, marginLeft: 140},
                ]}
              >
                14:00
              </Text>
            </View>
          </View>

          <Text style={styles.header}>Detail Informasi Pengajuan </Text>
          <Text style={styles.contentTitle}>Jenis Klaim</Text>
          <Text style={styles.content}>Kesehatan </Text>
          <Text style={styles.contentTitle}>Nama Karyawan</Text>
          <Text style={styles.content}>Faiz</Text>
          <Text style={styles.contentTitle}>Tanggal Pengajuan</Text>
          <Text style={styles.content}>10 Des 2020</Text>
          <Text style={styles.contentTitle}>Jumlah</Text>
          <Text style={styles.content}>Rp300.000</Text>
          <Text style={styles.contentTitle}>Vendor</Text>
          <Text style={styles.content}>Starbucks</Text>
          <Text style={styles.contentTitle}>Alamat</Text>
          <Text style={styles.content}>Jawa Tengah</Text>
          <Text style={styles.contentTitle}>Nota</Text>
          <Image source={nota} style={{width: 180, height: 450}} />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  wrap: {
    marginTop: -20,
  },
  img: {
    marginTop: 40,
    width: 24,
    height: 24,
    marginLeft: 20,
  },
  ellipse: {
    marginTop: 30,
    borderRadius: 50,
    width: 12,
    height: 12,
    marginLeft: 8,
  },
  header: {
    fontFamily: "Poppins-Regular",
    fontSize: 14,
    color: "#FFC200",
    marginTop: 36,
    marginLeft: 2,
    marginBottom: 10,
    fontWeight: "bold",
  },
  text: {
    fontFamily: "Poppins-Regular",
    fontSize: 14,
    color: "#1F1F1F",
    marginTop: 36,
    marginLeft: 8,
  },
  contentTitle: {
    color: "#1F1F1F",
    fontSize: 14,
    fontFamily: "Poppins-Regular",
    marginTop: 8,
  },
  content: {
    color: "#828282",
    fontSize: 14,
    fontFamily: "Poppins-Regular",
    marginTop: 4,
  },
});

export default detailPengajuan;
