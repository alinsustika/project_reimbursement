import React from "react";
import {View, Text} from "react-native";

import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";

import masuk from "./Page/Masuk";
import daftar from "./Page/Daftar";
import home from "./Page/Home";
import riwayat from "./Page/riwayatPengajuan";
import riwayat_mingguIni from "./Page/riwayatPengajuan/mingguIni";
import riwayat_bulanIni from "./Page/riwayatPengajuan/bulanIni";
import listKlaim from "./Page/semuaStatusKlaim";
import ajuan from "./Page/Ajuan";
import detailPengajuanDiproses from "./Page/detailPengajuan/Diproses";
import detailPengajuanSelesai from "./Page/detailPengajuan/Selesai";
import profilEdit from "./Page/Profil/Edit";
import profilFix from "./Page/Profil/Fix";
import detailPengajuan_Admin from "./Page/Admin/DetailPengajuan";
import home_Admin from "./Page/Admin/Home";
import ListNotif from "./Page/Notif/ListNotif";
import DetailNotif from "./Page/Notif/DetailNotif";
import DetailListNotif from "./Page/Notif/DetailListNotif";
import detailPengajuan_HRD from "./Page/HRD/DetailPengajuan";
import home_HRD from "./Page/HRD/Home";
import detailPengajuan_Kabag from "./Page/Kabag/DetailPengajuan";
import home_Kabag from "./Page/Kabag/Home";

const RootStack = createStackNavigator();

export default function layarDepan() {
  return (
    <NavigationContainer>
      <RootStack.Navigator screenOptions={{headerShown: false}}>
        <RootStack.Screen name="masuk" component={masuk} />
        <RootStack.Screen name="daftar" component={daftar} />
        <RootStack.Screen name="home" component={home} />
        <RootStack.Screen name="profilEdit" component={profilEdit} />
        <RootStack.Screen name="profilFix" component={profilFix} />
        <RootStack.Screen name="riwayat" component={riwayat} />
        <RootStack.Screen
          name="riwayat_mingguIni"
          component={riwayat_mingguIni}
        />
        <RootStack.Screen
          name="riwayat_bulanIni"
          component={riwayat_bulanIni}
        />
        <RootStack.Screen name="listKlaim" component={listKlaim} />
        <RootStack.Screen name="ajuan" component={ajuan} />
        <RootStack.Screen
          name="detailPengajuanDiproses"
          component={detailPengajuanDiproses}
        />
        <RootStack.Screen
          name="detailPengajuanSelesai"
          component={detailPengajuanSelesai}
        />
        <RootStack.Screen
          name="detailPengajuan_Admin"
          component={detailPengajuan_Admin}
        />
        <RootStack.Screen name="home_Admin" component={home_Admin} />
        <RootStack.Screen
          name="detailPengajuan_HRD"
          component={detailPengajuan_HRD}
        />
        <RootStack.Screen name="home_HRD" component={home_HRD} />
        <RootStack.Screen
          name="detailPengajuan_Kabag"
          component={detailPengajuan_Kabag}
        />
        <RootStack.Screen name="home_Kabag" component={home_Kabag} />
        <RootStack.Screen name="ListNotif" component={ListNotif} />
        <RootStack.Screen name="DetailNotif" component={DetailNotif} />
        <RootStack.Screen name="DetailListNotif" component={DetailListNotif} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
}
